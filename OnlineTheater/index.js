const express = require("express");
const path = require("path");
const exphbs = require("express-handlebars");
const logger = require("./middleware/logger");
const neo4j = require("./routes/api/neo4j-api");
const random_name = require("random-name");
const Fakerator = require("fakerator");
const fakerator = Fakerator("en-CA");
var _ = require("lodash");
const {
  User,
  Name,
  Location,
  Coordinates,
  Timezone,
  Picture,
  DateOfBirth,
  DateOfRegistration,
} = require("./models/user");

var getUserByIdQuerry =
  "MATCH (u:User {id: $id})" +
  " MATCH (u:User)-[with_name:WITH_NAME]->(name:Name)" +
  " MATCH (u:User)-[lives_at:LIVES_AT]->(location:Location)-[has_coordinates:HAS_COORDINATES]->(coordinates:Coordinates)" +
  " MATCH (u:User)-[lives_at:LIVES_AT]->(location:Location)-[is_on_timezone:IS_ON_TIMEZONE]->(timezone:Timezone)" +
  " MATCH (u:User)-[was_born:WAS_BORN]->(dateOfBirth:DateOfBirth)" +
  " MATCH (u:User)-[was_register:WAS_REGISTERED]->(dateOfRegistration:DateOfRegistration)" +
  " MATCH (u:User)-[profile_pictures:PROFILE_PICTURES]->(picture:Picture)" +
  "WITH {id: u.id, api_key: u.api_key, username: u.username, password: u.password, gender: u.gender, email: u.email, nationality: u.nationality," +
  " title: name.title, firstName: name.firstName, lastName: name.lastName," +
  " street: location.street, city: location.city, state: location.state, postcode: location.postcode," +
  " latitude: coordinates.latitude, longitude: coordinates.longitude," +
  " offset: timezone.offset, description: timezone.description," +
  " dateOfBirth_Date: dateOfBirth.date, dateOfBirth_Age: dateOfBirth.age," +
  " dateOfRegistration_Date: dateOfRegistration.date, dateOfRegistration_Age: dateOfRegistration.age," +
  " large: picture.large, medium: picture.medium, thumbnail: picture.thumbnail }" +
  " AS resultedUser" +
  " RETURN resultedUser";

var getTheaterByIdQuerry =
  "MATCH (t:Theater {id: $id})" +
  " MATCH (t:Theater {id: $id})-[located_at:LOCATED_AT]->(location:Location)-[has_coordinates:HAS_COORDINATES]->(coordinates:Coordinates)" +
  " MATCH (t:Theater {id: $id})-[located_at:LOCATED_AT]->(location:Location)-[is_on_timezone:IS_ON_TIMEZONE]->(timezone:Timezone)" +
  " MATCH (t:Theater {id: $id})-[with_seating_plan:WITH_SEATING_PLAN]->(seatingPlan: SeatingPlan)" +
  "WITH {id: t.id, name: t.name, phone: t.phone, logoUrl: t.logoUrl, nearestBusStation: t.nearestBusStation, capacity: t.capacity, descriptionTitle: t.descriptionTitle, descriptions: t.descriptions, facilities: t.facilities," +
  " street: location.street, city: location.city, state: location.state, postcode: location.postcode," +
  " latitude: coordinates.latitude, longitude: coordinates.longitude," +
  " offset: timezone.offset, description: timezone.description," +
  " seatingPlanName: seatingPlan.seatingPlanName, seatingPlanLink: seatingPlan.seatingPlanLink }" +
  " AS resultedTheater" +
  " RETURN resultedTheater";

var getPerformanceByIdQuerry =
  "MATCH (p:Performance {id: $id})" +
  " MATCH (p:Performance {id: $id})-[with_details:WITH_DETAILS]->(details:Details)" +
  " MATCH (p:Performance {id: $id})-[has_ratings:HAS_RATINGS]->(ratings:Ratings)" +
  " MATCH (p:Performance {id: $id})-[with_age_restriction:WITH_AGE_RESTRICTION]->(ageRestriction: AgeRestriction)" +
  "WITH {id: p.id, name: p.name, synopsis: p.synopsis," +
  " previewsFrom: details.previewsFrom, openingDate: details.openingDate, bookingFrom: details.bookingFrom, avaibleUntil: details.avaibleUntil, runTime: details.runTime," +
  " rating: ratings.rating, review: ratings.review," +
  " ageRating: ageRestriction.ageRating, ageRatingImageUrl: ageRestriction.ageRatingImageUrl, ageAdvisory: ageRestriction.ageAdvisory }" +
  " AS resultedPerformance" +
  " RETURN resultedPerformance";

var delleteDatabase = function (session) {
  return session.run("MATCH (n) DETACH DELETE n").then((results) => {});
};

var getUserById = function (session, id) {
  return session.run(getUserByIdQuerry, { id: id }).then((results) => {
    console.log("Records", results.records[0]._fields[0]);

    var modifiedUser = {};

    modifiedUser._id = results.records[0]._fields[0].id;

    modifiedUser.username = results.records[0]._fields[0].username;
    modifiedUser.password = results.records[0]._fields[0].password;
    modifiedUser.api_key = results.records[0]._fields[0].api_key;
    modifiedUser.gender = results.records[0]._fields[0].gender;
    modifiedUser.email = results.records[0]._fields[0].email;
    modifiedUser.nationality = results.records[0]._fields[0].nationality;

    //User - Name
    modifiedUser.title = results.records[0]._fields[0].title;
    modifiedUser.firstName = results.records[0]._fields[0].firstName;
    modifiedUser.lastName = results.records[0]._fields[0].lastName;

    //User - Location
    modifiedUser.street = results.records[0]._fields[0].street;
    modifiedUser.city = results.records[0]._fields[0].city;
    modifiedUser.state = results.records[0]._fields[0].state;
    modifiedUser.postcode = results.records[0]._fields[0].postcode;

    //User - Location - Coordinates
    modifiedUser.latitude = results.records[0]._fields[0].latitude;
    modifiedUser.longitude = results.records[0]._fields[0].longitude;

    //User - Location - Timezone
    modifiedUser.offset = results.records[0]._fields[0].offset;
    modifiedUser.description = results.records[0]._fields[0].description;

    //User - DateOfBirth
    modifiedUser.dateOfBirth_Date = results.records[0]._fields[0].dateOfBirth_Date;
    modifiedUser.dateOfBirth_Age = results.records[0]._fields[0].dateOfBirth_Age;

    //User - DateOfRegistration
    modifiedUser.dateOfRegistration_Date = results.records[0]._fields[0].dateOfRegistration_Date;
    modifiedUser.dateOfRegistration_Age = results.records[0]._fields[0].dateOfRegistration_Age;

    //User - Picture
    modifiedUser.large = results.records[0]._fields[0].large;
    modifiedUser.medium = results.records[0]._fields[0].medium;
    modifiedUser.thumbnail = results.records[0]._fields[0].thumbnail;

    return modifiedUser;
  });
};

var getTheaterById = function (session, id) {
  return session.run(getTheaterByIdQuerry, { id: id }).then((results) => {
    console.log("Records", results.records[0]._fields[0]);

    var modifiedTheater = {};

    modifiedTheater._id = results.records[0]._fields[0].id;

    modifiedTheater.name = results.records[0]._fields[0].name;
    modifiedTheater.phone = results.records[0]._fields[0].phone;
    modifiedTheater.logoUrl = results.records[0]._fields[0].logoUrl;
    modifiedTheater.nearestBusStation = results.records[0]._fields[0].nearestBusStation;
    modifiedTheater.capacity = results.records[0]._fields[0].capacity;
    modifiedTheater.descriptionTitle = results.records[0]._fields[0].descriptionTitle;
    modifiedTheater.descriptions = results.records[0]._fields[0].descriptions;
    modifiedTheater.facilities = results.records[0]._fields[0].facilities;

    modifiedTheater.street = results.records[0]._fields[0].street;
    modifiedTheater.city = results.records[0]._fields[0].city;
    modifiedTheater.state = results.records[0]._fields[0].state;
    modifiedTheater.postcode = results.records[0]._fields[0].postcode;

    modifiedTheater.latitude = results.records[0]._fields[0].latitude;
    modifiedTheater.longitude = results.records[0]._fields[0].longitude;

    modifiedTheater.offset = results.records[0]._fields[0].offset;
    modifiedTheater.description = results.records[0]._fields[0].description;

    modifiedTheater.seatingPlanName = results.records[0]._fields[0].seatingPlanName;
    modifiedTheater.seatingPlanLink = results.records[0]._fields[0].seatingPlanLink;

    return modifiedTheater;
  });
};

var getPerformanceById = function (session, id) {
  return session.run(getPerformanceByIdQuerry, { id: id }).then((results) => {
    console.log("Records", results.records[0]._fields[0]);

    var modifiedPerformance = {};

    modifiedPerformance.id = results.records[0]._fields[0].id;
    modifiedPerformance.name = results.records[0]._fields[0].name;
    modifiedPerformance.synopsis = results.records[0]._fields[0].synopsis;
    modifiedPerformance.previewsFrom = results.records[0]._fields[0].previewsFrom;
    modifiedPerformance.openingDate = results.records[0]._fields[0].openingDate;
    modifiedPerformance.bookingFrom = results.records[0]._fields[0].bookingFrom;
    modifiedPerformance.avaibleUntil = results.records[0]._fields[0].avaibleUntil;
    modifiedPerformance.runTime = results.records[0]._fields[0].runTime;
    modifiedPerformance.rating = results.records[0]._fields[0].rating;
    modifiedPerformance.review = results.records[0]._fields[0].review;
    modifiedPerformance.ageRating = results.records[0]._fields[0].ageRating;
    modifiedPerformance.ageRatingImageUrl = results.records[0]._fields[0].ageRatingImageUrl;
    modifiedPerformance.ageAdvisory = results.records[0]._fields[0].ageAdvisory;
    modifiedPerformance.created = results.records[0]._fields[0].created;

    return modifiedPerformance;
  });
};

var delleteUser = function (session, userId) {
  return session
    .run(
      "MATCH (user:User {id: $userId}) MATCH (user:User)-[r:LIVES_AT]->(location:Location) DETACH DELETE user, r, location",
      {
        userId: userId,
      }
    )
    .then((result) => {
      return session.run("MATCH (n) WHERE size((n)--())=0 DELETE  n");
    });
};

var delleteTheater = function (session, theaterId) {
  return session
    .run(
      "MATCH (theater:Theater {id: $theaterId}) MATCH (theater:Theater)-[r:LOCATED_AT]->(location:Location) DETACH DELETE theater, r, location",
      {
        theaterId: theaterId,
      }
    )
    .then((result) => {
      return session.run("MATCH (n) WHERE size((n)--())=0 DELETE  n");
    });
};

var delletePerformance = function (session, performanceId) {
  return session
    .run("MATCH (performance:Performance {id: $performanceId}) DETACH DELETE performance", {
      performanceId: performanceId,
    })
    .then((result) => {
      return session.run("MATCH (n) WHERE size((n)--())=0 DELETE  n");
    });
};

const app = express();

// Init middleware

// Handlebars Middleware
app.engine("handlebars", exphbs());
app.set("view engine", "handlebars");

// Body Parser Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Homepage Route
app.get("/", async function (req, res, next) {
  /*let result = await neo4j.get_num_nodes();
  console.log("NUMBER OF NODES IS", result);*/
  res.render("index", { alertType: "success", msg: "Default" });
});

// Set static folder
app.use(express.static(path.join(__dirname, "public")));

// Members API Routes
app.use("/api/users", require("./routes/api/users"));
app.use("/api/theaters", require("./routes/api/theaters"));
app.use("/api/performances", require("./routes/api/performances"));
app.use("/api/register", require("./routes/api/register"));
app.use("/api/create-update-theaters", require("./routes/api/create-update-theaters"));
app.use("/api/create-update-performances", require("./routes/api/create-update-performances"));
app.use("/api/login", require("./routes/api/login"));
app.use("/api/category", require("./routes/api/category"));

app.get("/api/delete-all-nodes", async (req, res) => {
  var session = neo4j.driver.session();
  await delleteDatabase(session);
  res.render("index", { alertType: "success", msg: "Database is now empty!!!" });
});

app.get("/api/create-update-theaters/update/:theaterId", async (req, res) => {
  var session = neo4j.driver.session();
  await delleteDatabase(session);
  res.render("index", { alertType: "success", msg: "Database is now empty!!!" });
});

app.get("/api/users/update/:userId", async (req, res) => {
  var userId = req.params.userId;
  var session = neo4j.driver.session();
  var user = await getUserById(session, userId);
  console.log("User", user);
  res.render("register", {
    userId,
    method: "update",
    title: "Update user",
    user,
  });
});

app.get("/api/users/delete/:userId", async (req, res) => {
  var id = req.params.userId;
  console.log(id);
  var session = neo4j.driver.session();
  await delleteUser(session, id);
  res.render("index", { alertType: "success", msg: "User has been deleted" });
});

app.get("/api/theaters/update/:theaterId", async (req, res) => {
  var theaterId = req.params.theaterId;
  var session = neo4j.driver.session();
  var theater = await getTheaterById(session, theaterId);
  console.log("Theater", theater);
  res.render("create-update-theaters", {
    theaterId,
    method: "update",
    title: "Update theater",
    theater,
  });
});

app.get("/api/performances/update/:performanceId", async (req, res) => {
  var performanceId = req.params.performanceId;
  console.log(performanceId);
  var session = neo4j.driver.session();
  var performance = await getPerformanceById(session, performanceId);
  console.log("Performance", performance);
  res.render("create-update-performances", {
    performanceId,
    method: "update",
    title: "Update performance",
    performance,
  });
});

app.get("/api/theaters/delete/:theaterId", async (req, res) => {
  var id = req.params.theaterId;
  console.log(id);
  var session = neo4j.driver.session();
  await delleteTheater(session, id);
  res.render("index", { alertType: "success", msg: "Theater has been deleted" });
});

app.get("/api/performances/delete/:performanceId", async (req, res) => {
  var id = req.params.performanceId;
  console.log(id);
  var session = neo4j.driver.session();
  await delletePerformance(session, id);
  res.render("index", { alertType: "success", msg: "Performance has been deleted" });
});

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));

const { User } = require("./user");

class PerformanceRatingAndRewiew {
  constructor() {
    this.score = 0;
    this.review = "";
  }
}

//nesto slicno
class PerformanceDescription {
  constructor() {
    this.number = "";
    this.short = "";
    this.long = "";
  }
}

class OtherTitles {
  constructor() {
    this.otherTitles = [
      {
        country: "",
        title: "",
      },
    ];
  }
}

class Category {
  constructor() {
    this.name = "";
  }
}

class AgeRating {
  constructor() {
    this.ageRating = "";
    this.ageRatingImageUrl = "";
    this.ageAdvisory = "";
  }
}

class PerformanceCastAndCreative {
  constructor() {
    this.createdBy = new User();
    this.producer = [];
    this.director = new User();
  }
}

class Performance {
  constructor() {
    this.id = "";
    this.name = "";
    this.synopsis = "";
    this.previewsFrom = new Date();
    this.openingDate = new Date();
    this.bookingFrom = new Date();
    this.avaibleUntil = new Date();
    this.runTime = "";
    this.categories = [
      {
        categoryName: "",
      },
    ];
    this.ratingAndRewiew = PerformanceRatingAndRewiew();
    this.ageRating = new AgeRating();
    this.castAndCreative = new this.castAndCreative();
  }
}

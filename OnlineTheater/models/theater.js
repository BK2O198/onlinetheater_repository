var Location = require("./user").Location;

class SeatingPlan {
  constructor() {
    this.name = "";
    this.planUrl = "";
  }
}

class Theater {
  constructor() {
    this.id = "";
    this.name = "";
    this.adress = new Location();
    this.phone = "";
    this.logoUrl = "";
    this.seatingPlan = new SeatingPlan();
    this.nearestBusStation = "";
    this.capacity = "";
    this.descriptions = [
      {
        title: "",
        description: "",
      },
    ];
    this.facilities = "";
    this.productions = [
      {
        show: "",
        opened: "",
        closed: "",
        link: "",
      },
    ];
  }
}

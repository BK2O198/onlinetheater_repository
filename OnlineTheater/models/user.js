class Name {
  constructor() {
    this.title = "";
    this.firstName = "";
    this.lastName = "";
  }
}

class Coordinates {
  constructor() {
    this.latitude = "";
    this.longitude = "";
  }
}

class Timezone {
  constructor() {
    this.offset = "";
    this.description = "";
  }
}

class Location {
  constructor() {
    this.street = "";
    this.city = "";
    this.state = "";
    this.postcode = "";
    this.coordinates = new Coordinates();
    this.timezone = new Timezone();
  }
}

function getAge(dateString) {
  var today = new Date();
  var birthDate = new Date(dateString);
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  return age;
}

class DateOfBirth {
  constructor() {
    this.date = "";
    this.age = 0;
  }
}

class DateOfRegistration {
  constructor() {
    this.date = "";
    this.age = 0;
  }
}

class Picture {
  constructor() {
    this.large = "";
    this.medium = "";
    this.thumbnail = "";
  }
}

class User {
  constructor() {
    this.id = "";
    this.auth_token = "";
    this.gender = "";
    this.name = new Name();
    this.location = new Location();
    this.email = "";
    this.dateOfBirth = new DateOfBirth();
    this.dateOfRegistration = new DateOfRegistration();
    this.phones = [];
    this.cellPhones = [];
    this.picture = new Picture();
    this.nationality = "";
  }
}

class PhoneNumber {
  constructor(phoneType, description, number) {
    this.phoneType = phoneType;
    this.description = description;
    this.number = number;
  }
}

module.exports = {
  User,
  Name,
  Coordinates,
  Timezone,
  Location,
  DateOfBirth,
  DateOfRegistration,
  Picture,
  getAge,
  PhoneNumber,
};

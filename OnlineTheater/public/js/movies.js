var data = {
  films: [
    {
      film_id: 311084,
      imdb_id: 9784798,
      imdb_title_id: "tt9784798",
      film_name: "Judas and the Black Messiah",
      other_titles: null,
      release_dates: [
        {
          release_date: "2021-02-12",
          notes: "Nationwide",
        },
      ],
      age_rating: [
        {
          rating: "R ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/r.png",
          age_advisory: "for violence and pervasive language",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/311084_high_V2.mp4",
      synopsis_long:
        "FBI informant William O'Neal infiltrates the Illinois Black Panther Party and is tasked with keeping tabs on their charismatic leader, Chairman Fred Hampton. A career thief, O'Neal revels in the danger of manipulating both his comrades and his handler, Special Agent Roy Mitchell. Hampton's political prowess grows just as he's falling in love with fellow revolutionary Deborah Johnson. Meanwhile, a battle wages for O'Neal's soul. Will he align with the forces of good? Or subdue Hampton and The Panthers by any means, as FBI Director J. Edgar Hoover commands?",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/311084/311084h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/311084/311084h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 308811,
      imdb_id: 10016180,
      imdb_title_id: "tt10016180",
      film_name: "The Little Things",
      other_titles: null,
      release_dates: [
        {
          release_date: "2021-01-29",
          notes: "Nationwide",
        },
      ],
      age_rating: [
        {
          rating: "R ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/r.png",
          age_advisory: "for violent/disturbing images, language and full nudity",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/308811_high.mp4",
      synopsis_long:
        "Kern County Deputy Sheriff Joe \"Deke\" Deacon is sent to Los Angeles for what should have been a quick evidence-gathering assignment. Instead, he becomes embroiled in the search for a serial killer who is terrorizing the city. Leading the hunt, L.A. Sheriff Department Sergeant Jim Baxter, impressed with Deke's cop instincts, unofficially engages his help. But as they track the killer, Baxter is unaware that the investigation is dredging up echoes of Deke's past, uncovering disturbing secrets that could threaten more than his case.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/308811/308811h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/308811/308811h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 189712,
      imdb_id: 2850386,
      imdb_title_id: "tt2850386",
      film_name: "The Croods: A New Age",
      other_titles: null,
      release_dates: [
        {
          release_date: "2020-11-25",
          notes: "Nationwide",
        },
      ],
      age_rating: [
        {
          rating: "PG ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/pg.png",
          age_advisory: "for peril, action and rude humor",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/189712_high.mp4",
      synopsis_long:
        "The Croods have survived their fair share of dangers and disasters, from fanged prehistoric beasts to surviving the end of the world, but now they will face their biggest challenge of all: another family. The Croods need a new place to live. So, the first prehistoric family sets off into the world in search of a safer place to call home. When they discover an idyllic walled-in paradise that meets all their needs, they think their problems are solved  except for one thing. Another family already lives there: the Bettermans.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/189712/189712h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/189712/189712h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 314959,
      imdb_id: 6902332,
      imdb_title_id: "tt6902332",
      film_name: "The Marksman",
      other_titles: null,
      release_dates: [
        {
          release_date: "2021-01-15",
          notes: "Nationwide",
        },
      ],
      age_rating: [
        {
          rating: "PG-13 ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/pg-13.png",
          age_advisory: "for violence, some bloody images and brief strong language.",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/314959_high.mp4",
      synopsis_long:
        "Hardened Arizona rancher Jim Hanson simply wants to be left alone as he fends off eviction notices and tries to make a living on an isolated stretch of borderland. But everything changes when Hanson, an ex-Marine sharpshooter, witnesses 11-year-old migrant Miguel fleeing with his mother Rosa from drug cartel assassins led by the ruthless Mauricio.  After being caught in a shoot-out, a dying Rosa begs Jim to take her son to safety to her family in Chicago. Defying his cop daughter Sarah, Jim sneaks Miguel out of the local U.S. Customs and Border Patrol station and together, they hit the road with the group of killers in pursuit.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/314959/314959h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/314959/314959h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 315334,
      imdb_id: 10265034,
      imdb_title_id: "tt10265034",
      film_name: "Land",
      other_titles: null,
      release_dates: [
        {
          release_date: "2021-02-12",
          notes: "Limited",
        },
      ],
      age_rating: [
        {
          rating: "PG-13 ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/pg-13.png",
          age_advisory: "",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/315334_high.mp4",
      synopsis_long:
        "From acclaimed actress Robin Wright comes her directorial debut LAND, the poignant story of one woman's search for meaning in the vast and harsh American wilderness. Edee (Wright), in the aftermath of an unfathomable event, finds herself unable to stay connected to the world she once knew and in the face of that uncertainty, retreats to the magnificent, but unforgiving, wilds of the Rockies. After a local hunter (Demián Bichir) brings her back from the brink of death, she must find a way to live again.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/315334/315334h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/315334/315334h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 254343,
      imdb_id: 7126948,
      imdb_title_id: "tt7126948",
      film_name: "Wonder Woman 1984",
      other_titles: null,
      release_dates: [
        {
          release_date: "2020-12-25",
          notes: "Nationwide",
        },
      ],
      age_rating: [
        {
          rating: "PG-13 ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/pg-13.png",
          age_advisory: "for sequences of action and violence",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/254343_high_V2.mp4",
      synopsis_long:
        "Fast forward to the 1980s as Wonder Woman's (Gal Gadot) next big screen adventure finds her facing an all-new foe: The Cheetah (Kristen Wiig).",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/254343/254343h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/254343/254343h2.jpg",
              width: 300,
              height: 199,
            },
          },
        },
      },
    },
    {
      film_id: 287656,
      imdb_id: 6475714,
      imdb_title_id: "tt6475714",
      film_name: "Monster Hunter",
      other_titles: null,
      release_dates: [
        {
          release_date: "2020-12-18",
          notes: "Nationwide",
        },
      ],
      age_rating: [
        {
          rating: "PG-13 ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/pg-13.png",
          age_advisory: "for sequences of creature action and violence throughout",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/287656_high.mp4",
      synopsis_long:
        "Behind our world, there is another: a world of dangerous and powerful monsters that rule their domain with deadly ferocity.  When Lt. Artemis (Milla Jovovich) and her loyal soldiers are transported from our world to the new world, the unflappable lieutenant receives the shock of her life.  In her desperate battle for survival against enormous enemies with incredible powers and unstoppable, terrifying attacks, Artemis will team up with a mysterious man (Tony Jaa) who has found a way to fight back.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/287656/287656h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/287656/287656h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 299932,
      imdb_id: 6878306,
      imdb_title_id: "tt6878306",
      film_name: "News of the World",
      other_titles: null,
      release_dates: [
        {
          release_date: "2020-12-25",
          notes: "Nationwide",
        },
      ],
      age_rating: [
        {
          rating: "PG-13 ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/pg-13.png",
          age_advisory: "for violence, disturbing images, thematic material and some language.",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/299932_high.mp4",
      synopsis_long:
        "Five years after the end of the Civil War, Captain Jefferson Kyle Kidd, a widower and veteran of three wars, now moves from town to town as a non-fiction storyteller, sharing the news of presidents and queens, glorious feuds, devastating catastrophes, and gripping adventures from the far reaches of the  globe. In the plains of Texas, he crosses paths with Johanna, a 10-year-old taken in by the Kiowa tribe six years earlier and raised as one of their own. Johanna, hostile to a world she's never experienced, is being returned to her biological aunt and uncle against her will. Kidd agrees to deliver the child where the law says she belongs. As they travel hundreds of miles into the unforgiving wilderness, the two will face tremendous challenges of both human and natural forces as they search for a place that either can call home.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/299932/299932h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/299932/299932h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 314927,
      imdb_id: 4761112,
      imdb_title_id: "tt4761112",
      film_name: "The Mauritanian",
      other_titles: null,
      release_dates: [
        {
          release_date: "2021-02-12",
          notes: "Platform",
        },
      ],
      age_rating: [
        {
          rating: "R ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/r.png",
          age_advisory: "for violence including a sexual assault, and language.",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/314927_high.mp4",
      synopsis_long:
        'Directed by Kevin Macdonald and based on the NY Times best-selling memoir "Guantánamo Diary" by Mohamedou Ould Slahi, this is the inspiring true story of Slahi\'s fight for freedom after being detained and imprisoned without charge by the U.S. Government for years. Alone and afraid, Slahi (Tahar Rahim) finds allies in defense attorney Nancy Hollander (Jodie Foster) and her associate Teri Duncan (Shailene Woodley) who battle the U.S. government in a fight for justice that tests their commitment to the law and their client at every turn. Their controversial advocacy, along with evidence uncovered by a formidable military prosecutor, Lt. Colonel Stuart Couch (Benedict Cumberbatch), uncovers shocking truths and ultimately proves that the human spirit cannot be locked up.',
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/314927/314927h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/314927/314927h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 305468,
      imdb_id: 10633456,
      imdb_title_id: "tt10633456",
      film_name: "Minari",
      other_titles: null,
      release_dates: [
        {
          release_date: "2021-02-12",
          notes: "Limited",
        },
      ],
      age_rating: [
        {
          rating: "PG-13 ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/pg-13.png",
          age_advisory: "for some thematic elements and a rude gesture",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/305468_high.mp4",
      synopsis_long:
        "A tender and sweeping story about what roots us, Minari follows a Korean-American family that moves to a tiny Arkansas farm in search of their own American Dream. The family home changes completely with the arrival of their sly, foul-mouthed, but incredibly loving grandmother. Amidst the instability and challenges of this new life in the rugged Ozarks, Minari shows the undeniable resilience of family and what really makes a home.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/305468/305468h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
      },
    },
    {
      film_id: 311844,
      imdb_id: 9738716,
      imdb_title_id: "tt9738716",
      film_name: "The World to Come",
      other_titles: null,
      release_dates: [
        {
          release_date: "2021-02-12",
          notes: "Limited",
        },
      ],
      age_rating: [
        {
          rating: "R ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/r.png",
          age_advisory: "for some sexuality/nudity.",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/311844_high.mp4",
      synopsis_long:
        "In this powerful 19th century romance set in the American Northeast, Abigail, a farmer's wife, and her new neighbor Tallie find themselves irrevocably drawn to each other. A grieving Abigail tends to her withdrawn husband Dyer as free spirit Tallie bristles at the jealous control of her husband Finney. Together, their intimacy begins to fill a void in each other's lives they never knew existed.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/311844/311844h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/311844/311844h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 218226,
      imdb_id: 4846340,
      imdb_title_id: "tt4846340",
      film_name: "Hidden Figures",
      other_titles: null,
      release_dates: [
        {
          release_date: "2016-12-25",
          notes: "Limited",
        },
        {
          release_date: "2017-01-06",
          notes: "Nationwide",
        },
      ],
      age_rating: [
        {
          rating: "PG ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/pg.png",
          age_advisory: "for thematic elements and some language",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/218226_high.mp4",
      synopsis_long:
        "The incredible untold story of Katherine G. Johnson, Dorothy Vaughan and Mary Jackson - brilliant African-American women working at NASA, who served as the brains behind one of the greatest operations in history: the launch of astronaut John Glenn into orbit, a stunning achievement that restored the nation's confidence, turned around the Space Race, and galvanized the world.  The visionary trio crossed all gender and race lines to inspire generations to dream big.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/218226/218226h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/218226/218226h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 235235,
      imdb_id: 4532038,
      imdb_title_id: "tt4532038",
      film_name: "The War with Grandpa",
      other_titles: null,
      release_dates: [
        {
          release_date: "2020-10-09",
          notes: "Nationwide",
        },
      ],
      age_rating: [
        {
          rating: "PG ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/pg.png",
          age_advisory: "for rude humor, language, and some thematic elements",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/235235_high_V2.mp4",
      synopsis_long:
        "Sixth-grader Peter (Oakes Fegley) is pretty much your average kid-he likes gaming, hanging with his friends and his beloved pair of Air Jordans. But when his recently widowed grandfather Ed (Robert De Niro) moves in with Peter's family, the boy is forced to give up his most prized possession of all, his bedroom. Unwilling to let such an injustice stand, Peter devises a series of increasingly elaborate pranks to drive out the interloper, but Grandpa Ed won't go without a fight. Soon, the friendly combatants are engaged in an all-out war with side-splitting consequences.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/235235/235235h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/235235/235235h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 298876,
      imdb_id: 8829830,
      imdb_title_id: "tt8829830",
      film_name: "Fatale",
      other_titles: null,
      release_dates: [
        {
          release_date: "2020-12-18",
          notes: "Nationwide",
        },
      ],
      age_rating: [
        {
          rating: "R ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/r.png",
          age_advisory: "for violence, sexual content and language.",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/298876_high.mp4",
      synopsis_long:
        "After a wild one-night stand, Derrick (Michael Ealy), a successful sports agent, watches his perfect life slowly disappear when he discovers that the sexy and mysterious woman he risked everything for, is a determined police detective (Hilary Swank) who entangles him in her latest investigation. As he tries desperately to put the pieces together, he falls deeper into her trap, risking his family, his career, and even his life. FATALE is a suspenseful and provocative psychological thriller and an unpredictable game of cat and mouse where one mistake can change your life.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/298876/298876h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/298876/298876h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 287879,
      imdb_id: 9770150,
      imdb_title_id: "tt9770150",
      film_name: "Nomadland",
      other_titles: null,
      release_dates: [
        {
          release_date: "2021-02-19",
          notes: "Limited",
        },
      ],
      age_rating: [
        {
          rating: "R ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/r.png",
          age_advisory: "for some full nudity",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/287879_high_V2.mp4",
      synopsis_long:
        "Following the economic collapse of a company town in rural Nevada, Fern (Frances McDormand) packs her van and sets off on the road exploring a life outside of conventional society as a modern-day nomad. The third feature film from director Chloé Zhao, NOMADLAND features real nomads Linda May, Swankie and Bob Wells as Fern's mentors and comrades in her exploration through the vast landscape of the American West.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/287879/287879h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/287879/287879h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 42445,
      imdb_id: 332280,
      imdb_title_id: "tt0332280",
      film_name: "The Notebook (2004)",
      other_titles: null,
      release_dates: [
        {
          release_date: "2004-06-25",
          notes: "Nationwide",
        },
      ],
      age_rating: [
        {
          rating: "PG-13 ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/pg-13.png",
          age_advisory: "for some sexuality",
        },
      ],
      film_trailer: null,
      synopsis_long:
        "An epic love story centered around an older man who reads aloud to an older, invalid woman whom he regularly visits. From a faded notebook, the old man's words bring to life the story about a couple who is separated by World War II, and is then passionately reunited, seven years later, after they have taken different paths. Though her memory has faded, his words give her the chance to relive her turbulent youth and the unforgettable love they shared.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/42445/042445h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/42445/042445h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 304067,
      imdb_id: 9620292,
      imdb_title_id: "tt9620292",
      film_name: "Promising Young Woman",
      other_titles: null,
      release_dates: [
        {
          release_date: "2020-12-25",
          notes: "Nationwide",
        },
      ],
      age_rating: [
        {
          rating: "R ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/r.png",
          age_advisory:
            "for strong violence including sexual assault, language throughout, some sexual material and drug use",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/304067_high_V2.mp4",
      synopsis_long:
        "From visionary director Emerald Fennell (Killing Eve) comes a delicious new take on revenge. Everyone said Cassie (Carey Mulligan) was a promising young woman... until a mysterious event abruptly derailed her future.  But nothing in Cassie's life is what it appears to be: she's wickedly smart, tantalizingly cunning, and she's living a secret double life by night.  Now, an unexpected encounter is about to give Cassie a chance to right the wrongs of the past in this thrilling and wildly entertaining story.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/304067/304067h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/304067/304067h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 316144,
      imdb_id: 7541720,
      imdb_title_id: "tt7541720",
      film_name: "Music",
      other_titles: null,
      release_dates: [
        {
          release_date: "2021-02-12",
          notes: "Limited",
        },
      ],
      age_rating: [
        {
          rating: "PG-13 ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/pg-13.png",
          age_advisory: "for thematic content, drug material, brief violence and strong language",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/316144_high.mp4",
      synopsis_long:
        "Zu, a free spirit estranged from her family, suddenly finds herself the sole guardian of her half-sister, Music, a teenager on the autism spectrum whose whole world order had been beautifully crafted by her late grandmother. With a history of addiction issues that challenge her self-worth and reliability, Zu can barely take care of herself and she struggles with the new responsibilities her sister brings. But Music has the watchful loving eye of her local community, and Zu soon learns that life's obstacles can be made easier with a little help from their neighbor, Ebo.  Music explores the fragile bonds that hold us together and imagines a world where love can be found in the most miraculous of places.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/316144/316144h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/316144/316144h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 311592,
      imdb_id: 9608818,
      imdb_title_id: "tt9608818",
      film_name: "Our Friend",
      other_titles: null,
      release_dates: [
        {
          release_date: "2021-01-22",
          notes: "Limited",
        },
      ],
      age_rating: [
        {
          rating: "R ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/r.png",
          age_advisory: "for language.",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/311592_high.mp4",
      synopsis_long:
        "OUR FRIEND tells the inspiring and extraordinary true story of the Teague family - journalist Matt (Casey Affleck), his vibrant wife Nicole (Dakota Johnson) and their two young daughters - and how their lives are upended by Nicole's heartbreaking diagnosis of terminal cancer. As Matt's responsibilities as caretaker and parent become increasingly overwhelming, the couple's best friend Dane Faucheux (Jason Segel) offers to come and help out. As Dane puts his life on hold to stay with his friends, the impact of this life altering decision proves greater and more profound than anyone could have imagined.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/311592/311592h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/311592/311592h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 312021,
      imdb_id: 11169050,
      imdb_title_id: "tt11169050",
      film_name: "Supernova",
      other_titles: null,
      release_dates: [
        {
          release_date: "2021-01-29",
          notes: "Limited",
        },
      ],
      age_rating: [
        {
          rating: "R ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/r.png",
          age_advisory: "for language.",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/312021_high.mp4",
      synopsis_long:
        "Sam (Colin Firth) and Tusker (Stanley Tucci), partners of twenty years, are travelling across England in their old campervan visiting friends, family and places from their past. Following a life-changing diagnosis, their time together has become more important than ever until secret plans test their love like never before.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/312021/312021h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/312021/312021h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 309309,
      imdb_id: 8004664,
      imdb_title_id: "tt8004664",
      film_name: "Come Play",
      other_titles: null,
      release_dates: [
        {
          release_date: "2020-10-30",
          notes: "Limited",
        },
      ],
      age_rating: [
        {
          rating: "PG-13 ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/pg-13.png",
          age_advisory: "for terror, frightening images and some language",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/309309_high.mp4",
      synopsis_long:
        "Newcomer Azhy Robertson stars as Oliver, a lonely young boy who feels different from everyone else.  Desperate for a friend, he seeks solace and refuge in his ever-present cell phone and tablet.  When a mysterious creature uses Oliver's devices against him to break into our world, Oliver's parents (Gillian Jacobs and John Gallagher Jr.) must fight to save their son from the monster beyond the screen.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/309309/309309h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/309309/309309h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 314756,
      imdb_id: 9685342,
      imdb_title_id: "tt9685342",
      film_name: "A Writer's Odyssey",
      other_titles: null,
      release_dates: [
        {
          release_date: "2021-02-12",
          notes: "Limited",
        },
      ],
      age_rating: [
        {
          rating: "OTHER ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/other.png",
          age_advisory: "",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/314756_high_V2.mp4",
      synopsis_long:
        "A Writer's Odyssey, adapted from the novel of the same name, tells the story of Kongwen Lu (DONG Zijian), the author of a fantasy novel series following a heroic teenager, also named Kongwen, on a quest to end the tyrannical rule of Lord Redmane, under the guidance of a Black Armour (GUO Jingfei). But through a strange twist of fate, the fantasy world of the novels begins to impact life in the real world, leading Guan Ning (LEI Jiayin) to accept a mission from Tu Ling (YANG Mi) to kill the author.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/314756/314756h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/314756/314756h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 314247,
      imdb_id: 8114980,
      imdb_title_id: "tt8114980",
      film_name: "Willy's Wonderland",
      other_titles: null,
      release_dates: [
        {
          release_date: "2021-02-12",
          notes: "Limited",
        },
      ],
      age_rating: [
        {
          rating: "0 ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/uc.png",
          age_advisory: "",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/314247_high.mp4",
      synopsis_long:
        "A quiet loner (Nic Cage) finds himself stranded in a remote town when his car breaks down. Unable to pay for the repairs he needs, he agrees to spend the night cleaning Willy's Wonderland, an abandoned family fun center. But this wonderland has a dark secret that the \"The Janitor\" is about to discover. He soon finds himself trapped inside Willy's and locked in an epic battle with the possessed animatronic mascots that roam the halls. To survive, he must fight his way through each of them.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/314247/314247h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/314247/314247h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
    {
      film_id: 245041,
      imdb_id: 3104988,
      imdb_title_id: "tt3104988",
      film_name: "Crazy Rich Asians",
      other_titles: null,
      release_dates: [
        {
          release_date: "2018-08-15",
          notes: "Nationwide",
        },
      ],
      age_rating: [
        {
          rating: "PG-13 ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/pg-13.png",
          age_advisory: "for some suggestive content and language",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/245041_high.mp4",
      synopsis_long:
        "\"Crazy Rich Asians\" follows native New Yorker Rachel Chu (Wu) as she accompanies her longtime boyfriend, Nick Young (Golding), to his best friend's wedding in Singapore.  Excited about visiting Asia for the first time but nervous about meeting Nick's family, Rachel is unprepared to learn that Nick has neglected to mention a few key details about his life.  It turns out that he is not only the scion of one of the country's wealthiest families but also one of its most sought-after bachelors.  Being on Nick's arm puts a target on Rachel's back, with jealous socialites and, worse, Nick's own disapproving mother (Yeoh) taking aim.  And it soon becomes clear that while money can't buy love, it can definitely complicate things.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/245041/245041h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/245041/245041h2.jpg",
              width: 300,
              height: 199,
            },
          },
        },
      },
    },
    {
      film_id: 311784,
      imdb_id: 9182964,
      imdb_title_id: "tt9182964",
      film_name: "The Reckoning",
      other_titles: null,
      release_dates: [
        {
          release_date: "2021-02-05",
          notes: "Platform",
        },
      ],
      age_rating: [
        {
          rating: "0 ",
          age_rating_image: "https://assets.movieglu.com/age_rating_logos/us/uc.png",
          age_advisory: "",
        },
      ],
      film_trailer: "https://trailer.movieglu.com/311784_high.mp4",
      synopsis_long:
        "After losing her husband during the Great Plague, Grace Haverstock (Charlotte Kirk) is unjustly accused of being a witch and placed in the custody of England's most ruthless witch-hunter, Judge Moorcroft (Sean Pertwee). Forced to endure physical and emotional torture while steadfastly maintaining her innocence, Grace must face her own inner demons as the Devil himself starts to work his way into her mind.",
      images: {
        poster: {
          1: {
            image_orientation: "portrait",
            region: "global",
            medium: {
              film_image: "https://image.movieglu.com/311784/311784h1.jpg",
              width: 200,
              height: 300,
            },
          },
        },
        still: {
          1: {
            image_orientation: "landscape",
            medium: {
              film_image: "https://image.movieglu.com/311784/311784h2.jpg",
              width: 300,
              height: 200,
            },
          },
        },
      },
    },
  ],
  status: {
    count: 25,
    state: "OK",
    method: "filmsNowShowing",
    message: null,
    request_method: "GET",
    version: "ELFAv200",
    territory: "US",
    device_datetime_sent: "2021-02-12T11:31:23.116Z",
    device_datetime_used: "2021-02-12 11:31:23",
  },
};

module.exports = data;

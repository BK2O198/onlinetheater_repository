const express = require("express");
const { result } = require("lodash");
const routerCategory = express.Router();
const { v4: uuidv4 } = require("uuid");
const neo4j = require("./neo4j-api");
var _ = require("lodash");

var create = function (session, category) {
  return session
    .run(query, {
      performanceName: category.performanceName,
      name: category.name,
      created: created,
    })
    .then((results) => {
      console.log(results);

      return {
        alertType: "success",
        msg: "Added new category",
        status: null,
      };
    });
};

routerCategory.get("/", async (req, res, next) => {});

routerCategory.get("/:id", async (req, res, next) => {});

routerCategory.post("/", async (req, res, next) => {
  if (req.body.method === "actor") {
    console.log(req.body);
    var session = neo4j.driver.session();
    const query =
      "MATCH (p:Performance {name: $name})" +
      "MATCH (user:User {username: $username})" +
      "CREATE (p)<-[acts_in:ACTS_IN]-(user)" +
      " RETURN p, acts_in, user";
    session
      .run(query, {
        name: req.body.performance,
        username: req.body.actor,
      })
      .then((result) => {
        console.log(result);
      });
    res.render("index", { alertType: "success", msg: "Performance and actor conneced" });
  } else if (req.body.method === "director") {
    console.log(req.body);
    var session = neo4j.driver.session();
    const query =
      "MATCH (p:Performance {name: $name})" +
      "MATCH (user:User {username: $username})" +
      "CREATE (p)-[directed_by:DIRECTED_BY]->(user)" +
      " RETURN p, directed_by, user";
    session
      .run(query, {
        name: req.body.performance,
        username: req.body.director,
      })
      .then((result) => {
        console.log(result);
      });
    res.render("index", { alertType: "success", msg: "Performance and director conneced" });
  } else if (req.body.method === "creator") {
    console.log(req.body);
    var session = neo4j.driver.session();
    const query =
      "MATCH (p:Performance {name: $name})" +
      "MATCH (user:User {username: $username})" +
      "CREATE (p)-[created_by:CREATED_BY]->(user)" +
      " RETURN p, created_by, user";
    session
      .run(query, {
        name: req.body.performance,
        username: req.body.creator,
      })
      .then((result) => {
        console.log(result);
      });
    res.render("index", { alertType: "success", msg: "Performance and creator conneced" });
  } else if (req.body.method === "producer") {
    console.log(req.body);
    var session = neo4j.driver.session();
    const query =
      "MATCH (p:Performance {name: $name})" +
      "MATCH (user:User {username: $username})" +
      "CREATE (p)-[produced_by:PRODUCED_BY]->(user)" +
      " RETURN p, produced_by, user";
    session
      .run(query, {
        name: req.body.performance,
        username: req.body.producer,
      })
      .then((result) => {
        console.log(result);
      });
    res.render("index", { alertType: "success", msg: "Performance and producer conneced" });
  } else {
    console.log(req.body);
    var category = {
      performanceName: req.body.performance,
      name: req.body.name,
    };
    var session = neo4j.driver.session();
    session
      .run(
        "MATCH (p: Performance {name: $name}) " +
          "CREATE (category: Category {performanceName: $name, name: $categoryName}) " +
          "CREATE (p)-[with_category:WITH_CATEGORY]->(category) " +
          "RETURN p, with_category, category",
        {
          name: category.performanceName,
          categoryName: category.name,
        }
      )
      .then((results) => {
        console.log(results);
      });
    res.render("index", { alertType: "success", msg: "Category added" });
  }
});

routerCategory.put("/:id", async (req, res, next) => {});

routerCategory.delete("/:id", async (req, res, next) => {});

module.exports = routerCategory;

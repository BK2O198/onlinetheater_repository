const express = require("express");
const { result } = require("lodash");
const routerCreateUpdatePerformances = express.Router();
const { v4: uuidv4 } = require("uuid");
const neo4j = require("./neo4j-api");
var _ = require("lodash");

const queryUpdatePerformance =
  "MATCH (p:Performance {id: $id})" +
  " MATCH (p:Performance {id: $id})-[with_details:WITH_DETAILS]->(details:Details)" +
  " MATCH (p:Performance {id: $id})-[has_ratings:HAS_RATINGS]->(ratings:Ratings)" +
  " MATCH (p:Performance {id: $id})-[with_age_restriction:WITH_AGE_RESTRICTION]->(ageRestriction: AgeRestriction)" +
  " SET p = $propsPerformance," +
  " details = $propsDetails," +
  " ratings = $propsRatings," +
  " ageRestriction = $propsAgeRestriction";

var update = function (session, modifiedPerformance, id) {
  console.log("update part");
  var propsPerformance = {
    id: modifiedPerformance.id,
    name: modifiedPerformance.name,
    synopsis: modifiedPerformance.synopsis,
  };
  var propsDetails = {
    previewsFrom: modifiedPerformance.previewsFrom,
    openingDate: modifiedPerformance.openingDate,
    bookingFrom: modifiedPerformance.bookingFrom,
    avaibleUntil: modifiedPerformance.avaibleUntil,
    runTime: modifiedPerformance.runTime,
  };
  var propsRatings = {
    rating: modifiedPerformance.rating,
    review: modifiedPerformance.review,
  };
  var propsAgeRestriction = {
    ageRating: modifiedPerformance.ageRating,
    ageRatingImageUrl: modifiedPerformance.ageRatingImageUrl,
    ageAdvisory: modifiedPerformance.ageAdvisory,
  };
  return session
    .run(queryUpdatePerformance, {
      id: id,
      propsPerformance: propsPerformance,
      propsDetails: propsDetails,
      propsRatings: propsRatings,
      propsAgeRestriction: propsAgeRestriction,
    })
    .then((result) => {
      console.log("dadasd");
      console.log(result);
      console.log("Updated ", modifiedPerformance.name);
      return;
    });
};

var create = function (session, newPerformance) {
  return session
    .run("MATCH (performance:Performance{name: $name}) RETURN performance", {
      name: newPerformance.name,
    })
    .then((results) => {
      if (!_.isEmpty(results.records)) {
        return {
          alertType: "danger",
          msg: "Performance with that name already exists",
          status: null,
        };
      } else {
        const query =
          "CREATE (performance: Performance {id: $id, name: $name, synopsis: $synopsis}) " +
          "CREATE (details: Details {previewsFrom: $previewsFrom, openingDate: $openingDate, bookingFrom: $bookingFrom, avaibleUntil: $avaibleUntil, runTime: $runTime}) " +
          "CREATE (ratings: Ratings {rating: $rating, review: $review}) " +
          "CREATE (ageRestriction: AgeRestriction {ageRating: $ageRating, ageRatingImageUrl: $ageRatingImageUrl, ageAdvisory: $ageAdvisory}) " +
          "CREATE (performance)-[with_details:WITH_DETAILS {created: $created}]->(details) " +
          "CREATE (performance)-[has_ratings:HAS_RATINGS {created: $created}]->(ratings) " +
          "CREATE (performance)-[with_age_restriction:WITH_AGE_RESTRICTION {created: $created}]->(ageRestriction) " +
          "RETURN performance, details, ratings, ageRestriction";

        return session
          .run(query, {
            id: uuidv4(),
            name: newPerformance.name,
            synopsis: newPerformance.synopsis,
            previewsFrom: newPerformance.previewsFrom,
            openingDate: newPerformance.openingDate,
            bookingFrom: newPerformance.bookingFrom,
            avaibleUntil: newPerformance.avaibleUntil,
            runTime: newPerformance.runTime,
            rating: newPerformance.rating,
            review: newPerformance.review,
            ageRating: newPerformance.ageRating,
            ageRatingImageUrl: newPerformance.ageRatingImageUrl,
            ageAdvisory: newPerformance.ageAdvisory,
            created: Date.now(),
          })
          .then((results) => {
            console.log("newPerformance", newPerformance);
            console.log(results);
            return {
              alertType: "success",
              msg: "Added new performance",
              status: null,
            };
          });
      }
    });
};

routerCreateUpdatePerformances.get("/", async (req, res, next) => {
  var date = Date.now();

  var performance = {
    name: "Hamilton",
    synopsis:
      "The seeds for Hamilton were planted when composer Lin-Manuel Miranda was taking a well-earned break from starring in another of his hit musicals, In the Heights. At the airport, he purchased a copy of the biography by Ron Chernow titled Alexander Hamilton. Within a few pages, Miranda was hooked an began to envision the life of the revolutionary as a musical.",
    previewsFrom: "6th December 2017",
    openingDate: "22nd December 2017",
    bookingFrom: "93027",
    avaibleUntil: "20.9267",
    runTime: "-7.9310",
    rating: "0",
    review:
      "What's left to be said about Hamilton that's not already been said? One of the central questions the show asks is precisely who the storytellers are of our personal and collective histories: Who Lives, Who Dies, Who Tells Your Story, as the final number asks of the life story the show has just relayed and replayed of Alexander Hamilton, one of the key players in defining and refining the American constitution. ",
    ageRating: "16",
    ageRatingImageUrl:
      "https://www.londontheatre.co.uk/sites/default/files/styles/thumbnail/public/api/5388-1573115024-hamiltonsq071119.jpg?itok=EeHytuX1",
    ageAdvisory:
      "HAMILTON contains some strong language, the show is appropriate for ages 10+. Children under 4 are not permitted into the theatre.As an advisory to adults who might bring young people: All persons under the age of 16 must be accompanied by and sat next to the accompanying adult. They may not sit on their own within the auditorium. If children do have separate seats, you could be refused entry. All persons entering the theatre, regardless of age, must have a ticket.",
  };
  console.log("pass 1");
  res.render("create-update-performances", { title: "Create new performance", method: "create", performance });
});

routerCreateUpdatePerformances.get("/:id", async (req, res, next) => {});

routerCreateUpdatePerformances.post("/", async (req, res, next) => {
  var method = req.body.method;
  console.log("Body: ", req.body);

  if (method === "update") {
    var modifiedPerformance = {};

    modifiedPerformance.id = req.body.id;
    modifiedPerformance.name = req.body.name;
    modifiedPerformance.synopsis = req.body.synopsis;
    modifiedPerformance.previewsFrom = req.body.previewsFrom;
    modifiedPerformance.openingDate = req.body.openingDate;
    modifiedPerformance.bookingFrom = req.body.bookingFrom;
    modifiedPerformance.avaibleUntil = req.body.avaibleUntil;
    modifiedPerformance.runTime = req.body.runTime;
    modifiedPerformance.rating = req.body.rating;
    modifiedPerformance.review = req.body.review;
    modifiedPerformance.ageRating = req.body.ageRating;
    modifiedPerformance.ageRatingImageUrl = req.body.ageRatingImageUrl;
    modifiedPerformance.ageAdvisory = req.body.ageAdvisory;
    modifiedPerformance.created = req.body.created;
    console.log("MODIFIEDD", modifiedPerformance);

    var session = neo4j.driver.session();
    var id = req.body.id;

    await update(session, modifiedPerformance, id);
    res.render("index", { alertType: "success", msg: "Performance successfully updated!", status: null });
  } else {
    var newPerformance = {
      name: req.body.name,
      synopsis: req.body.synopsis,
      previewsFrom: req.body.previewsFrom,
      openingDate: req.body.openingDate,
      bookingFrom: req.body.bookingFrom,
      avaibleUntil: req.body.avaibleUntil,
      runTime: req.body.runTime,
      rating: req.body.rating,
      review: req.body.review,
      ageRating: req.body.ageRating,
      ageRatingImageUrl: req.body.ageRatingImageUrl,
      ageAdvisory: req.body.ageAdvisory,
    };
    console.log("Pass 2");
    console.log(newPerformance);

    var session = neo4j.driver.session();
    var sessionResult = await create(session, newPerformance);
    console.log(sessionResult);
    var result = sessionResult;
    if (result.msg === "Performance with that name already exists") {
      console.log("1111");
      console.log(result);
      res.render("create-update-performances", {
        title: "Create new performancee",
        alertType: result.alertType,
        msg: result.msg,
        status: result.status,
        performance: newPerformance,
      });
    } else {
      console.log("222");
      res.render("index", { alertType: result.alertType, msg: result.msg, status: result.status });
    }
  }
});

routerCreateUpdatePerformances.put("/:id", async (req, res, next) => {});

routerCreateUpdatePerformances.delete("/:id", async (req, res, next) => {});

module.exports = routerCreateUpdatePerformances;

module.exports = routerCreateUpdatePerformances;

const express = require("express");
const { result } = require("lodash");
const routerCreateUpdateTheaters = express.Router();
const { v4: uuidv4 } = require("uuid");
const neo4j = require("./neo4j-api");
var _ = require("lodash");

const queryUpdateUser =
  "MATCH (t:Theater {id: $id})" +
  " MATCH (t:Theater {id: $id})-[located_at:LOCATED_AT]->(location:Location)-[has_coordinates:HAS_COORDINATES]->(coordinates:Coordinates)" +
  " MATCH (t:Theater {id: $id})-[located_at:LOCATED_AT]->(location:Location)-[is_on_timezone:IS_ON_TIMEZONE]->(timezone:Timezone)" +
  " MATCH (t:Theater {id: $id})-[with_seating_plan:WITH_SEATING_PLAN]->(seatingPlan: SeatingPlan)" +
  " SET t = $propsTheater," +
  " location = $propsLocation," +
  " coordinates = $propsCoordinates," +
  " timezone = $propsTimezone," +
  " seatingPlan = $propsSeatingPlan";

var update = function (session, modifiedTheather, id) {
  var propsTheater = {
    id: modifiedTheather.id,
    name: modifiedTheather.name,
    phone: modifiedTheather.phone,
    logoUrl: modifiedTheather.logoUrl,
    nearestBusStation: modifiedTheather.nearestBusStation,
    capacity: modifiedTheather.capacity,
    descriptionTitle: modifiedTheather.descriptionTitle,
    descriptions: modifiedTheather.descriptions,
    facilities: modifiedTheather.facilities,
  };
  var propsLocation = {
    street: modifiedTheather.street,
    city: modifiedTheather.city,
    state: modifiedTheather.state,
    postcode: modifiedTheather.postcode,
  };
  var propsCoordinates = {
    latitude: modifiedTheather.latitude,
    longitude: modifiedTheather.longitude,
  };
  var propsTimezone = {
    offset: modifiedTheather.offset,
    description: modifiedTheather.description,
  };
  var propsSeatingPlan = {
    seatingPlanName: modifiedTheather.seatingPlanName,
    seatingPlanLink: modifiedTheather.seatingPlanLink,
  };
  return session
    .run(queryUpdateUser, {
      id: id,
      propsTheater: propsTheater,
      propsLocation: propsLocation,
      propsCoordinates: propsCoordinates,
      propsTimezone: propsTimezone,
      propsSeatingPlan: propsSeatingPlan,
    })
    .then((result) => {
      console.log(result);
      console.log("Updated ", modifiedTheather.name);
      return;
    });
};

var create = function (session, newTheater) {
  return session
    .run("MATCH (theater:Theater {name: $name}) RETURN theater", {
      name: newTheater.name,
    })
    .then((results) => {
      if (!_.isEmpty(results.records)) {
        return {
          alertType: "danger",
          msg: "Theater with that name already exists",
          status: null,
        };
      } else {
        const query =
          "CREATE (theater: Theater {id: $id, name: $name, phone: $phone, logoUrl: $logoUrl, nearestBusStation: $nearestBusStation, capacity: $capacity, descriptionTitle: $descriptionTitle, descriptions: $descriptions, facilities: $facilities}) " +
          "CREATE (location: Location {street: $street, city: $city, state: $state, postcode: $postcode}) " +
          "CREATE (coordinates: Coordinates {latitude: $latitude, longitude: $longitude}) " +
          "CREATE (timezone: Timezone {offset: $offset, description: $description}) " +
          "CREATE (seatingPlan: SeatingPlan {seatingPlanName: $seatingPlanName, seatingPlanLink: $seatingPlanLink}) " +
          "CREATE (theater)-[located_at:LOCATED_AT {created: $created}]->(location) " +
          "CREATE (location)-[has_coordinates:HAS_COORDINATES {created: $created}]->(coordinates) " +
          "CREATE (location)-[is_on_timezone:IS_ON_TIMEZONE {created: $created}]->(timezone) " +
          "CREATE (theater)-[with_seating_plan:WITH_SEATING_PLAN {created: $created}]->(seatingPlan) " +
          "RETURN theater, location, coordinates, timezone, seatingPlan";

        return session
          .run(query, {
            id: uuidv4(),
            name: newTheater.name,
            phone: newTheater.phone,
            logoUrl: newTheater.logoUrl,
            nearestBusStation: newTheater.nearestBusStation,
            capacity: newTheater.capacity,
            descriptionTitle: newTheater.descriptionTitle,
            descriptions: newTheater.descriptions,
            facilities: newTheater.facilities,
            street: newTheater.street,
            city: newTheater.city,
            state: newTheater.state,
            postcode: newTheater.postcode,
            latitude: newTheater.latitude,
            longitude: newTheater.longitude,
            offset: newTheater.offset,
            description: newTheater.description,
            seatingPlanName: newTheater.seatingPlanName,
            seatingPlanLink: newTheater.seatingPlanLink,
            created: Date.now(),
          })
          .then((results) => {
            console.log("newTheater", newTheater);
            console.log(results);
            return {
              alertType: "success",
              msg: "Added new theater",
              status: null,
            };
          });
      }
    });
};

routerCreateUpdateTheaters.get("/", async (req, res, next) => {
  var date = Date.now();

  var theater = {
    name: "Adelphi Theatre",
    street: "Victoria Street",
    city: "Kilcoole",
    state: "waterford",
    postcode: "93027",
    latitude: "20.9267",
    longitude: "-7.9310",
    offset: "-3:30",
    description: "Newfoundland",
    phone: "(0871) 224 4007",
    logoUrl: "https://d2z9fe5yu2p0av.cloudfront.net/chainlogos/uk/UK-614-sq.jpg",
    seatingPlanName: "Victoria Palace Theatre Seating Plan",
    seatingPlanLink: "https://www.londontheatre.co.uk/theatres/victoria-palace-theatre#seating-plan",
    nearestBusStation: "Victoria",
    capacity: "1436",
    descriptionTitle: "Adelphi Theatre",
    descriptions:
      "The Adelphi Theatre London that stands today opened its doors in December 1930, designed by the architect Ernest Schaufelberg. The first production to open in this theatre was Evergreen, a musical by Lorenz Hart and Richard Rodgers. The art deco features created during this time are still apparent today, having been extensively restored in a 1993 renovation. This building is actually the fourth theatre to reside on the site, the first of which opened its doors on 27th November 1806 under the name The Sans Pareil. The Sans Pareils was built by John Scott, a private businessman who built the theatre for his daughter so that she would have a place to perform and have the experience of managing a theatre. The theatre was refurbished in 1819 and renamed the Adelphi. In this incarnation the theatre became associated with burlettas, a form of entertainment where a straight play was substituted with songs and musical accompaniment to get round legal restrictions on the public performance of legitimate dramas.",
    facilities: "Air conditioned, Bar, Disabled toilets, Infrared hearing loop, Toilets, Wheelchair accessible",
    productions: [
      {
        show: "Back to the Future",
        opened: "August 2021",
        closed: "March 2020",
        link: "https://www.londontheatre.co.uk/show/back-to-the-future",
      },
      {
        show: "Waitress",
        opened: "February 2019",
        closed: "March 2020",
        link: "https://www.londontheatre.co.uk/reviews/review-waitress-at-the-adelphi-theatre",
      },
      {
        show: "Kinky Boots",
        opened: "August 2015",
        closed: "January 2019",
        link: "https://www.londontheatre.co.uk/reviews/kinky-boots-review",
      },
    ],
  };
  console.log("pass 1");
  res.render("create-update-theaters", { title: "Create new theater", theater });
});

routerCreateUpdateTheaters.get("/:id", async (req, res, next) => {});

routerCreateUpdateTheaters.post("/", async (req, res, next) => {
  var method = req.body.method;
  console.log("Body: ", req.body);

  if (method === "update") {
    var modifiedTheater = {};

    modifiedTheater.id = req.body._id;

    modifiedTheater.name = req.body.name;
    modifiedTheater.phone = req.body.phone;
    modifiedTheater.logoUrl = req.body.logoUrl;
    modifiedTheater.nearestBusStation = req.body.nearestBusStation;
    modifiedTheater.capacity = req.body.capacity;
    modifiedTheater.descriptionTitle = req.body.descriptionTitle;
    modifiedTheater.descriptions = req.body.descriptions;
    modifiedTheater.facilities = req.body.facilities;

    modifiedTheater.street = req.body.street;
    modifiedTheater.city = req.body.city;
    modifiedTheater.state = req.body.state;
    modifiedTheater.postcode = req.body.postcode;

    modifiedTheater.latitude = req.body.latitude;
    modifiedTheater.longitude = req.body.longitude;

    modifiedTheater.offset = req.body.offset;
    modifiedTheater.description = req.body.description;

    modifiedTheater.seatingPlanName = req.body.seatingPlanName;
    modifiedTheater.seatingPlanLink = req.body.seatingPlanLink;
    console.log("MODIFIEDD", modifiedTheater);

    var session = neo4j.driver.session();
    var id = req.body._id;

    await update(session, modifiedTheater, id);

    res.render("index", { alertType: "success", msg: "Theater successfully updated!", status: null });
  } else {
    var newTheater = {
      name: req.body.name,
      street: req.body.street,
      city: req.body.city,
      state: req.body.state,
      postcode: req.body.postcode,
      latitude: req.body.latitude,
      longitude: req.body.longitude,
      offset: req.body.offset,
      description: req.body.description,
      phone: req.body.phone,
      logoUrl: req.body.logoUrl,
      seatingPlanName: req.body.seatingPlanName,
      seatingPlanLink: req.body.seatingPlanLink,
      nearestBusStation: req.body.nearestBusStation,
      capacity: req.body.capacity,
      descriptionTitle: req.body.descriptionTitle,
      descriptions: req.body.descriptions,
      facilities: req.body.facilities,
    };
    console.log("Pass 2");
    console.log(newTheater);

    var session = neo4j.driver.session();
    var sessionResult = await create(session, newTheater);
    console.log(sessionResult);
    var result = sessionResult;
    if (result.msg === "Theater with that name already exists") {
      res.render("create-update-theaters", {
        title: "Create new theater",
        alertType: result.alertType,
        msg: result.msg,
        status: result.status,
        theater: newTheater,
      });
    } else {
      res.render("index", { alertType: result.alertType, msg: result.msg, status: result.status });
    }
  }
});

routerCreateUpdateTheaters.put("/:id", async (req, res, next) => {});

routerCreateUpdateTheaters.delete("/:id", async (req, res, next) => {});

module.exports = routerCreateUpdateTheaters;

module.exports = routerCreateUpdateTheaters;

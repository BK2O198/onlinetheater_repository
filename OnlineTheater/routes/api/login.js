const express = require("express");
const routerLogin = express.Router();
const hashPassword = require("../../public/js/hashFunctions").hashPassword;
const neo4j = require("./neo4j-api");
var _ = require("lodash");

var profile = function (session, apiKey) {
  return session
    .run("MATCH (user:User {api_key: $api_key}) RETURN user", {
      api_key: apiKey,
    })
    .then((results) => {
      if (_.isEmpty(results.records)) {
        return {
          alertType: "danger",
          msg: "Invalid authorization key",
          status: null,
        };
      }
      //return new User(results.records[0].get("user"));
      return results.records[0].get("user");
    });
};

var login = function (session, username, password) {
  return session
    .run("MATCH (user:User {username: $username}) RETURN user", {
      username: username,
    })
    .then((results) => {
      if (_.isEmpty(results.records)) {
        return {
          alertType: "danger",
          msg: "Username does not exist",
          status: null,
        };
      } else {
        var dbUser = _.get(results.records[0].get("user"), "properties");
        if (dbUser.password != hashPassword(username, password)) {
          return {
            alertType: "danger",
            msg: "Wrong password",
            status: null,
          };
        }
        return {
          token: _.get(dbUser, "api_key"),
        };
      }
    });
};

routerLogin.get("/", async (req, res, next) => {
  res.render("login", { title: "Login" });
});

routerLogin.get("/:id", async (req, res, next) => {});

routerLogin.post("/", async (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;
  var session = neo4j.driver.session();
  let result = await login(session, username, password);
  //let result = await profile(session, "958d74e6fb6d6530ece1");
  console.log(result);
  if (result.msg === "Username does not exist") {
    res.render("login", {
      title: "Login",
      alertType: "danger",
      msg: "Username does not exist",
      status: null,
    });
  } else if (result.msg === "Wrong password") {
    res.render("login", { title: "Login", alertType: "danger", msg: "Wrong password", status: null });
  } else {
    res.render("index", { alertType: "info", msg: "User logged in", status: null });
  }
});

routerLogin.put("/:id", async (req, res, next) => {});

routerLogin.delete("/:id", async (req, res, next) => {});

module.exports = routerLogin;

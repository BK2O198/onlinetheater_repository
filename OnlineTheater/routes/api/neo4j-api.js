let neo4j = require("neo4j-driver");
let driver = neo4j.driver("bolt://localhost:11003", neo4j.auth.basic("neo4j", "OnlineTheaterDBpassword"));

get_num_nodes = async function () {
  let session = driver.session();
  const num_nodes = await session.run("MATCH (n) RETURN n", {});
  session.close();
  console.log("RESULT", !num_nodes ? 0 : num_nodes.records.length);
  return !num_nodes ? 0 : num_nodes.records.length;
};

module.exports = {
  driver,
  get_num_nodes,
};

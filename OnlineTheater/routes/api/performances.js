const express = require("express");
const { result } = require("lodash");
const routerPerformances = express.Router();
const neo4j = require("./neo4j-api");

var getPerformances = function (session) {
  return session
    .run(
      "MATCH (p:Performance)" +
        " MATCH (p:Performance)-[with_details:WITH_DETAILS]->(details:Details)" +
        " MATCH (p:Performance)-[has_ratings:HAS_RATINGS]->(ratings:Ratings)" +
        " MATCH (p:Performance)-[with_age_restriction:WITH_AGE_RESTRICTION]->(ageRestriction: AgeRestriction)" +
        "WITH {id: p.id, name: p.name, synopsis: p.synopsis," +
        " previewsFrom: details.previewsFrom, openingDate: details.openingDate, bookingFrom: details.bookingFrom, avaibleUntil: details.avaibleUntil, runTime: details.runTime," +
        " rating: ratings.rating, review: ratings.review," +
        " ageRating: ageRestriction.ageRating, ageRatingImageUrl: ageRestriction.ageRatingImageUrl, ageAdvisory: ageRestriction.ageAdvisory }" +
        " AS resultedPerformance" +
        " RETURN resultedPerformance"
    )
    .then((results) => {
      var a = [];
      results.records.forEach((element) => {
        a.push(element._fields[0]);
      });
      const filteredArr = a.reduce((acc, current) => {
        const x = acc.find((item) => item.id === current.id);
        if (!x) {
          return acc.concat([current]);
        } else {
          return acc;
        }
      }, []);
      return filteredArr;
    });
};

routerPerformances.get("/", async (req, res, next) => {
  var session = neo4j.driver.session();
  var performances = await getPerformances(session);
  console.log(performances);
  res.render("performances", { performances });
});

routerPerformances.get("/:id", async (req, res, next) => {});

routerPerformances.post("/", async (req, res, next) => {});

routerPerformances.put("/:id", async (req, res, next) => {});

routerPerformances.delete("/:id", async (req, res, next) => {});

module.exports = routerPerformances;

module.exports = routerPerformances;

const express = require("express");
const routerRegister = express.Router();
const hashPassword = require("../../public/js/hashFunctions").hashPassword;
const UserCredentials = require("../../models/user-credentials");
const randomstring = require("randomstring");
const User = require("../../models/user").User;
const Name = require("../../models/user").Name;
const Location = require("../../models/user").Location;
const Coordinates = require("../../models/user").Coordinates;
const Timezone = require("../../models/user").Timezone;
const DateOfBirth = require("../../models/user").DateOfBirth;
const DateOfRegistration = require("../../models/user").DateOfRegistration;
const Picture = require("../../models/user").Picture;
const getAge = require("../../models/user").getAge;
const PhoneNumber = require("../../models/user").PhoneNumber;
const { v4: uuidv4 } = require("uuid");
const neo4j = require("./neo4j-api");
var _ = require("lodash");

const queryUpdateUser =
  "MATCH (user: User {id: $id})" +
  " MATCH (user:User {id: $id})-[with_name:WITH_NAME]->(name:Name)" +
  " MATCH (user:User {id: $id})-[lives_at:LIVES_AT]->(location:Location)-[has_coordinates:HAS_COORDINATES]->(coordinates:Coordinates)" +
  " MATCH (user:User {id: $id})-[lives_at:LIVES_AT]->(location:Location)-[is_on_timezone:IS_ON_TIMEZONE]->(timezone:Timezone)" +
  " MATCH (user:User {id: $id})-[was_born:WAS_BORN]->(dateOfBirth:DateOfBirth)" +
  " MATCH (user:User {id: $id})-[was_register:WAS_REGISTERED]->(dateOfRegistration:DateOfRegistration)" +
  " MATCH (user:User {id: $id})-[profile_pictures:PROFILE_PICTURES]->(picture:Picture)" +
  " SET user = $propsUser," +
  " name = $propsName," +
  " location = $propsLocation," +
  " coordinates = $propsCoordinates," +
  " timezone = $propsTimezone," +
  " dateOfBirth = $propsDateOfBirth," +
  " dateOfRegistration =$propsDateOfRegistration," +
  " picture = $propsPicture";

var update = function (session, modifiedUser, id) {
  var propsUser = {
    id: id,
    api_key: modifiedUser.api_key,
    password: modifiedUser.password,
    username: modifiedUser.username,
    gender: modifiedUser.gender,
    email: modifiedUser.email,
    nationality: modifiedUser.nationality,
  };
  var propsName = {
    title: modifiedUser.title,
    firstName: modifiedUser.firstName,
    lastName: modifiedUser.lastName,
  };
  var propsLocation = {
    street: modifiedUser.street,
    city: modifiedUser.city,
    state: modifiedUser.state,
    postcode: modifiedUser.postcode,
  };
  var propsCoordinates = {
    latitude: modifiedUser.latitude,
    longitude: modifiedUser.longitude,
  };
  var propsTimezone = {
    offset: modifiedUser.offset,
    description: modifiedUser.description,
  };
  var propsDateOfBirth = {
    date: modifiedUser.dateOfBirth_Date,
    age: modifiedUser.dateOfBirth_Age,
  };
  var propsDateOfRegistration = {
    date: modifiedUser.dateOfRegistration_Date,
    age: modifiedUser.dateOfRegistration_Age,
  };
  var propsPicture = {
    large: modifiedUser.large,
    medium: modifiedUser.medium,
    thumbnail: modifiedUser.thumbnail,
  };
  return session
    .run(queryUpdateUser, {
      id: id,
      propsUser: propsUser,
      propsName: propsName,
      propsLocation: propsLocation,
      propsCoordinates: propsCoordinates,
      propsTimezone: propsTimezone,
      propsDateOfBirth: propsDateOfBirth,
      propsDateOfRegistration: propsDateOfRegistration,
      propsPicture: propsPicture,
    })
    .then((result) => {
      console.log(result);
      console.log("Updated ", modifiedUser.username);
      return;
    });
};

var register = function (session, newUser) {
  return session
    .run("MATCH (user:User {username: $username}) RETURN user", {
      username: newUser.username,
    })
    .then((results) => {
      if (!_.isEmpty(results.records)) {
        return {
          alertType: "danger",
          msg: "Username already in use",
          status: null,
        };
      } else {
        var lastUpdated = Date.now();

        const query =
          "CREATE (user: User {id: $id, api_key: $api_key, username: $username, password: $password, gender: $gender, email: $email, nationality: $nationality}) " +
          "CREATE (name: Name {title: $title, firstName: $firstName, lastName: $lastName}) " +
          "CREATE (location: Location {street: $street, city: $city, state: $state, postcode: $postcode}) " +
          "CREATE (coordinates: Coordinates {latitude: $latitude, longitude: $longitude}) " +
          "CREATE (timezone: Timezone {offset: $offset, description: $description}) " +
          "CREATE (dateOfBirth: DateOfBirth {date: $dateOfBirth_Date, age: $dateOfBirth_Age}) " +
          "CREATE (dateOfRegistration: DateOfRegistration {date: $dateOfRegistration_Date, age: $dateOfRegistration_Age}) " +
          "CREATE (picture: Picture {large: $large, medium: $medium, thumbnail: $thumbnail})" +
          "CREATE (user)-[n:WITH_NAME {lastUpdated: $currentDate}]->(name) " +
          "CREATE (user)-[l:LIVES_AT {lastUpdated: $currentDate}]->(location) " +
          "CREATE (location)-[has_coordinates:HAS_COORDINATES {lastUpdated: $currentDate}]->(coordinates) " +
          "CREATE (location)-[is_on_timezone:IS_ON_TIMEZONE {lastUpdated: $currentDate}]->(timezone) " +
          "CREATE (user)-[b:WAS_BORN {lastUpdated: $currentDate}]->(dateOfBirth) " +
          "CREATE (user)-[r:WAS_REGISTERED {lastUpdated: $currentDate}]->(dateOfRegistration) " +
          "CREATE (user)-[p:PROFILE_PICTURES {lastUpdated: $currentDate}]->(picture) " +
          "RETURN user, name, location";

        return session
          .run(query, {
            id: uuidv4(),
            api_key: randomstring.generate({
              length: 20,
              charset: "hex",
            }),
            username: newUser.username,
            password: hashPassword(newUser.username, newUser.password),
            gender: newUser.gender,
            email: newUser.email,
            nationality: newUser.nationality,
            title: newUser.name.title,
            firstName: newUser.name.firstName,
            lastName: newUser.name.lastName,
            street: newUser.location.street,
            city: newUser.location.city,
            state: newUser.location.state,
            postcode: newUser.location.postcode,
            latitude: newUser.location.coordinates.latitude,
            longitude: newUser.location.coordinates.longitude,
            offset: newUser.location.timezone.offset,
            description: newUser.location.timezone.description,
            dateOfBirth_Date: newUser.dateOfBirth.date,
            dateOfBirth_Age: newUser.dateOfBirth.age,
            dateOfRegistration_Date: newUser.dateOfRegistration.date,
            dateOfRegistration_Age: newUser.dateOfRegistration.age,
            large: newUser.picture.large,
            medium: newUser.picture.medium,
            thumbnail: newUser.picture.thumbnail,
            currentDate: lastUpdated,
            lastUpdated: lastUpdated,
          })
          .then((results) => {
            console.log("newUser", newUser);
            console.log(results);
            return {
              alertType: "success",
              msg: "Added new user",
              status: null,
            };
          });
      }
    });
};

routerRegister.get("/", async (req, res, next) => {
  var date = Date.now();
  var user = {
    username: "BK2O198",
    password: "12345678",
    title: "mr",
    firstName: "Brad",
    lastName: "Gibson",
    gender: "male",
    email: "brad.gibson@example.com",
    street: "9278 new road",
    city: "Kilcoole",
    state: "waterford",
    postcode: "93027",
    latitude: "20.9267",
    longitude: "-7.9310",
    offset: "-3:30",
    description: "Newfoundland",
    dateOfBirth_Date: "1993-07-20T09:44:18.674Z",
    dateOfBirth_Age: 27,
    dateOfRegistration_Date: date,
    dateOfRegistration_Age: 0,
    nationality: "IE",
    large: "https://randomuser.me/api/portraits/men/75.jpg",
    medium: "https://randomuser.me/api/portraits/med/men/75.jpg",
    thumbnail: "https://randomuser.me/api/portraits/thumb/men/75.jpg",
  };
  res.render("register", { title: "Create new user", user });
});

routerRegister.get("/:id", async (req, res, next) => {});

routerRegister.post("/", async (req, res, next) => {
  var method = req.body.method;
  console.log("Body: ", req.body);

  if (method === "update") {
    var modifiedUser = {};

    modifiedUser.username = req.body.username;
    modifiedUser.password = req.body.password;
    modifiedUser.api_key = req.body.api_key;
    modifiedUser.gender = req.body.gender;
    modifiedUser.email = req.body.email;
    modifiedUser.nationality = req.body.nationality;

    //User - Name
    modifiedUser.title = req.body.title;
    modifiedUser.firstName = req.body.firstName;
    modifiedUser.lastName = req.body.lastName;

    modifiedUser.nationality = req.body.nationality;

    //User - Location
    modifiedUser.street = req.body.street;
    modifiedUser.city = req.body.city;
    modifiedUser.state = req.body.state;
    modifiedUser.postcode = req.body.postcode;

    //User - Location - Coordinates
    modifiedUser.latitude = req.body.latitude;
    modifiedUser.longitude = req.body.longitude;

    //User - Location - Timezone
    modifiedUser.offset = req.body.offset;
    modifiedUser.description = req.body.description;

    //User - DateOfBirth
    modifiedUser.dateOfBirth_Date = req.body.dateOfBirth_Date;
    modifiedUser.dateOfBirth_Age = req.body.dateOfBirth_Age;

    //User - DateOfRegistration
    modifiedUser.dateOfRegistration_Date = req.body.dateOfRegistration_Date;
    modifiedUser.dateOfRegistration_Age = req.body.dateOfRegistration_Age;

    //User - Picture
    modifiedUser.large = req.body.large;
    modifiedUser.medium = req.body.medium;
    modifiedUser.thumbnail = req.body.thumbnail;

    console.log(modifiedUser);

    var session = neo4j.driver.session();
    var id = req.body._id;

    await update(session, modifiedUser, id);

    res.render("index", { alertType: "success", msg: "User successfully updated!", status: null });
  } else {
    var newUser = new User();

    newUser.username = req.body.username;
    newUser.password = req.body.password;
    newUser.gender = req.body.gender;
    newUser.email = req.body.email;
    newUser.nationality = req.body.nationality;

    //User - Name
    var name = new Name();
    name.title = req.body.title;
    name.firstName = req.body.firstName;
    name.lastName = req.body.lastName;
    newUser.name = name;

    newUser.nationality = req.body.nationality;

    //User - Location
    var location = new Location();
    location.street = req.body.street;
    location.city = req.body.city;
    location.state = req.body.state;
    location.postcode = req.body.postcode;
    newUser.location = location;

    //User - Location - Coordinates
    var coordinates = new Coordinates();
    coordinates.latitude = req.body.latitude;
    coordinates.longitude = req.body.longitude;
    newUser.location.coordinates = coordinates;

    //User - Location - Timezone
    var timezone = new Timezone();
    timezone.offset = req.body.offset;
    timezone.description = req.body.description;
    newUser.location.timezone = timezone;

    //User - DateOfBirth
    var dateOfBirth = new DateOfBirth();
    dateOfBirth.date = req.body.dateOfBirth_Date;
    dateOfBirth.age = getAge(req.body.dateOfBirth_Date);
    newUser.dateOfBirth = dateOfBirth;

    //User - DateOfRegistration
    var dateOfRegistration = new DateOfRegistration();
    dateOfRegistration.date = Date.now();
    dateOfRegistration.age = 0;
    newUser.dateOfRegistration = dateOfRegistration;

    //User - Picture
    var picture = new Picture();
    picture.large = req.body.large;
    picture.medium = req.body.medium;
    picture.thumbnail = req.body.thumbnail;
    newUser.picture = picture;

    console.log(newUser);

    var session = neo4j.driver.session();
    var sessionResult = await register(session, newUser);
    console.log(sessionResult);
    var result = { msg: "Test result" };
    if (result.msg === "Username already in use") {
      res.render("register", {
        title: "Create new user",
        alertType: result.alertType,
        msg: result.msg,
        status: result.status,
      });
    } else {
      res.render("index", { alertType: result.alertType, msg: result.msg, status: result.status });
    }
  }
});

routerRegister.put("/:id", async (req, res, next) => {});

routerRegister.delete("/:id", async (req, res, next) => {});

module.exports = routerRegister;

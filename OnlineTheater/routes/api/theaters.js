const express = require("express");
const { result } = require("lodash");
const routerTheaters = express.Router();
const neo4j = require("./neo4j-api");

var getTheathers = function (session) {
  return session
    .run(
      "MATCH (t:Theater)" +
        " MATCH (t:Theater)-[located_at:LOCATED_AT]->(location:Location)-[has_coordinates:HAS_COORDINATES]->(coordinates:Coordinates)" +
        " MATCH (t:Theater)-[located_at:LOCATED_AT]->(location:Location)-[is_on_timezone:IS_ON_TIMEZONE]->(timezone:Timezone)" +
        " MATCH (t:Theater)-[with_seating_plan:WITH_SEATING_PLAN]->(seatingPlan: SeatingPlan)" +
        "WITH {id: t.id, name: t.name, phone: t.phone, logoUrl: t.logoUrl, nearestBusStation: t.nearestBusStation, capacity: t.capacity, descriptionTitle: t.descriptionTitle, descriptions: t.descriptions, facilities: t.facilities," +
        " street: location.street, city: location.city, state: location.state, postcode: location.postcode," +
        " latitude: coordinates.latitude, longitude: coordinates.longitude," +
        " offset: timezone.offset, description: timezone.description," +
        " seatingPlanName: seatingPlan.seatingPlanName, seatingPlanLink: seatingPlan.seatingPlanLink }" +
        " AS resultedTheater" +
        " RETURN resultedTheater"
    )
    .then((results) => {
      var a = [];
      results.records.forEach((element) => {
        a.push(element._fields[0]);
      });
      const filteredArr = a.reduce((acc, current) => {
        const x = acc.find((item) => item.id === current.id);
        if (!x) {
          return acc.concat([current]);
        } else {
          return acc;
        }
      }, []);
      return filteredArr;
    });
};

routerTheaters.get("/", async (req, res, next) => {
  var session = neo4j.driver.session();
  var theaters = await getTheathers(session);
  console.log(theaters);
  res.render("theaters", { theaters });
});

routerTheaters.get("/:id", async (req, res, next) => {});

routerTheaters.post("/", async (req, res, next) => {});

routerTheaters.put("/:id", async (req, res, next) => {});

routerTheaters.delete("/:id", async (req, res, next) => {});

module.exports = routerTheaters;

module.exports = routerTheaters;

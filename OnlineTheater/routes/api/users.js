const express = require("express");
const { result } = require("lodash");
const routerUsers = express.Router();
const neo4j = require("./neo4j-api");

var getUsers = function (session) {
  return session
    .run(
      "MATCH (u:User)" +
        " MATCH (u:User)-[with_name:WITH_NAME]->(name:Name)" +
        " MATCH (u:User)-[lives_at:LIVES_AT]->(location:Location)-[has_coordinates:HAS_COORDINATES]->(coordinates:Coordinates)" +
        " MATCH (u:User)-[lives_at:LIVES_AT]->(location:Location)-[is_on_timezone:IS_ON_TIMEZONE]->(timezone:Timezone)" +
        " MATCH (u:User)-[was_born:WAS_BORN]->(dateOfBirth:DateOfBirth)" +
        " MATCH (u:User)-[was_register:WAS_REGISTERED]->(dateOfRegistration:DateOfRegistration)" +
        " MATCH (u:User)-[profile_pictures:PROFILE_PICTURES]->(picture:Picture)" +
        "WITH {id: u.id, api_key: u.api_key, username: u.username, password: u.password, gender: u.gender, email: u.email, nationality: u.nationality," +
        " title: name.title, firstName: name.firstName, lastName: name.lastName," +
        " street: location.street, city: location.city, state: location.state, postcode: location.postcode," +
        " latitude: coordinates.latitude, longitude: coordinates.longitude," +
        " offset: timezone.offset, description: timezone.description," +
        " dateOfBirth_Date: dateOfBirth.date, dateOfBirth_Age: dateOfBirth.age," +
        " dateOfRegistration_Date: dateOfRegistration.date, dateOfRegistration_Age: dateOfRegistration.age," +
        " large: picture.large, medium: picture.medium, thumbnail: picture.thumbnail }" +
        " AS resultedUser" +
        " RETURN resultedUser"
    )
    .then((results) => {
      var a = [];
      results.records.forEach((element) => {
        a.push(element._fields[0]);
      });
      const filteredArr = a.reduce((acc, current) => {
        const x = acc.find((item) => item.id === current.id);
        if (!x) {
          return acc.concat([current]);
        } else {
          return acc;
        }
      }, []);
      console.log(filteredArr);
      return filteredArr;
    });
};

routerUsers.get("/", async (req, res, next) => {
  var session = neo4j.driver.session();
  var users = await getUsers(session);
  res.render("users", { users });
});

routerUsers.get("/:id", async (req, res, next) => {});

routerUsers.post("/", async (req, res, next) => {});

routerUsers.put("/:id", async (req, res, next) => {});

routerUsers.delete("/:id", async (req, res, next) => {});

module.exports = routerUsers;

module.exports = routerUsers;
